# OSVOS: One-Shot Video Object Segmentation
Check our [project page](http://www.vision.ee.ethz.ch/~cvlsegmentation/osvos) for additional information.
![OSVOS](doc/ims/osvos.png)

#### This repository was ported to PyTorch 0.4.0!

OSVOS is a method that tackles the task of semi-supervised video object segmentation. It is based on a fully-convolutional neural network architecture that is able to successively transfer generic semantic information, learned on ImageNet, to the task of foreground segmentation, and finally to learning the appearance of a single annotated object of the test sequence (hence one-shot). Experiments on DAVIS 2016 show that OSVOS is faster than currently available techniques and improves the state of the art by a significant margin (79.8% vs 68.0%).


This PyTorch code is a posteriori implementation of OSVOS and it does not contain the boundary snapping branch. The results published in the paper were obtained using the Caffe version that can be found at [OSVOS-caffe](https://github.com/kmaninis/OSVOS-caffe). TensorFlow implementation is also available at [OSVOS-TensorFlow](https://github.com/scaelles/OSVOS-TensorFlow).

Tested on Ubuntu 20.04 with NVIDIA 3090 Driver Version: 465.19.01 CUDA Version: 11.3 

### Installation:
1. Clone the OSVOS-PyTorch repository
   ```Shell
   git clone https://gitlab.com/harikrishnan.venkatesh/osvos-pytorch
   ```
2. **Python 3.8 required!**

3. Install dependencies (create virtualenv)
   
   `pip install -r requirements.txt`
   
   NOTE: wget and zip is needed

### Online training and testing
1. run `python prepare.py` to download the weights and dataset

1. Take a video you want to test it on and extract the frames from it using ffmpeg
   example:-
   ```Shell
    ffmpeg -i input_video.mp4 -r 1/1 -start_number 0 -s 854x480 DAVIS/JPEGImages/480p/<name_of_your_category>/%05d.jpg
   ```

1. The data is expected in this format ( similar to DAVIS dataset )

```   
DAVIS/
├── Annotations
│   ├── 1080p
│   │   ├── bear
│   │   ├── <name_of_your_category>
│   ├── 480p
│   │   ├── bear
│   │   ├── <name_of_your_category> !ADD BINARY IMAGE MASK HERE
│   └── db_info.yml
├── ImageSets
│   ├── 1080p
│   │   ├── train.txt
│   │   ├── trainval.txt
│   │   └── val.txt
│   └── 480p
│       ├── train.txt
│       ├── trainval.txt
│       └── val.txt
├── JPEGImages
│   ├── 1080p
│   │   ├── bear
│   │   ├── <name_of_your_category>
│   └── 480p
│       ├── bear
│       ├── <name_of_your_category> !ADD FRAMES OF VIDEO HERE
├── README.md
├── train_seqs.txt
└── val_seqs.txt
```

1. Annotate 1-5 frames of the video containing the object using Anosup and convert to binary mask using
   `python binary_mask_from_coco.py -i <anosup_json_path> -o <mask_output_path>`
   
1. Anosup might change the name of the files...it should be `00000.jpg`,`00001.jpg` etc...please use the `renumber_images.py` script to rename it.
   `python renumber_images.py -i <folder_containing images from anosup>`
   It will perform the renumber in place.

1. If your images are not the same size as 854x480, please use `resize_images.py`
   `python resize_images.py -i <folder_containing_images_to_be_resized>
   Please resize both masks and images.

5. Move the mask into `DAVIS/Annotations` folder
   
6. Run `python train_online.py -c <name_of_your_category>`.

7. Test output using `python test_model.py -c <name_of_your_category>`

### Training the parent network (optional)
1. All the training sequences of DAVIS 2016 are required to train the parent model, thus download them from [here](https://graphics.ethz.ch/Downloads/Data/Davis/DAVIS-data.zip).
2. Download the [VGG model](https://data.vision.ee.ethz.ch/kmaninis/share/OSVOS/Downloads/models/vgg_mat.zip) (55 MB) pretrained on ImageNet, and unzip it under `models/`, by running:
    ```Shell
    cd models/
    chmod +x download_vgg_weights.sh
    ./download_vgg_weights.sh
    cd ..
    ```
3. Place the files with the [train](https://github.com/kmaninis/OSVOS-PyTorch/files/1938742/train_seqs.txt) and [test](https://github.com/kmaninis/OSVOS-PyTorch/files/1938743/val_seqs.txt) sequences names in the DAVIS root folder (`db_root_dir()` in `mypath.py`).
4. Edit the 'User defined parameters' (eg. gpu_id) in file `train_parent.py`.
5. Run `train_parent.py`. This step takes 20 hours to train (Titan-X Pascal).

Enjoy!

### Citation:
	@Inproceedings{Cae+17,
	  Title          = {One-Shot Video Object Segmentation},
	  Author         = {S. Caelles and K.K. Maninis and J. Pont-Tuset and L. Leal-Taix\'e and D. Cremers and L. {Van Gool}},
	  Booktitle      = {Computer Vision and Pattern Recognition (CVPR)},
	  Year           = {2017}
	}
If you encounter any problems with the code, want to report bugs, etc. please contact us at {kmaninis, scaelles}[at]vision[dot]ee[dot]ethz[dot]ch.

