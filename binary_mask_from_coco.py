import argparse
from PIL import Image
from pathlib import Path
from pycocotools.coco import COCO

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', default='annotation.json')
parser.add_argument('-o', '--output', default='./binary_masks')
args = parser.parse_args()
annFile = args.input

output_path = Path(args.output)
if not output_path.exists():
    output_path.mkdir()

coco = COCO(annFile)
errors = 0
for img in coco.loadImgs(coco.imgs):
    annIds = coco.getAnnIds(imgIds=img['id'], iscrowd=None)
    anns = coco.loadAnns(annIds)

    try:
        mask = coco.annToMask(anns[0]) > 0
        for i in range(len(anns)):
            mask += coco.annToMask(anns[i]) > 0

        i = Image.fromarray(mask)
        i.save('{}/{}'.format(args.output, img["file_name"]))

    except:
        errors += 1
        print("error count : {}, img_id:{}".format(errors, img['id']))
