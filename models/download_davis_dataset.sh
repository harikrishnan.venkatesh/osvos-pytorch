#!/bin/bash

# Download the dataset
wget https://graphics.ethz.ch/Downloads/Data/Davis/DAVIS-data.zip
wget https://github.com/kmaninis/OSVOS-PyTorch/files/1938742/train_seqs.txt
wget https://github.com/kmaninis/OSVOS-PyTorch/files/1938743/val_seqs.txt
unzip DAVIS-data.zip
rm DAVIS-data.zip
mv DAVIS ../
mv train_seqs.txt ../DAVIS/
mv val_seqs.txt ../DAVIS/