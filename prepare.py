import subprocess
import os

os.chdir('./models')
subprocess.call(['bash', './download_vgg_weights.sh'])
subprocess.call(['bash', './download_parent_model.sh'])
subprocess.call(['bash', './download_davis_dataset.sh'])
