from pathlib import Path
from shutil import copy2
from PIL import Image
import os
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.description = "Script to renumber files/images inside a folder. Renumbers files to 00000.ext 00001.ext.... \
                If no output is specified, the input folder will be overwritten after confirmation"
parser.add_argument(
    "-i", "--input", help="input directory of images", required=True)
parser.add_argument(
    "-o", "--output", help="output directory of renumbered images, can be same as input")
parser.add_argument("-z", "--zero-pad",
                    help="number of places to pad eg: 5 -> 00001.jpg. default is 5", default=5)


def renumber_images(input_path, output_path, zero_pad, create_dir=False):
    if input_path == output_path:
        overwrite = input("do you want to overwrite the input folder y/n")
        print(overwrite)
        if overwrite != 'y' and overwrite != 'Y':
            exit(0)

        for idx, item in enumerate(tqdm(sorted(os.listdir(input_path)))):
            input_path = Path(input_path)
            input_file = input_path/item
            extension = input_file.suffix
            if input_file.is_file():
                os.rename(input_file, str(output_path /
                                          ("{0:0{i}d}{1}".format(idx, extension, i=zero_pad))))

    elif create_dir:
        if not output_path.exists():
            os.mkdir(output_path)

        for idx, item in enumerate(tqdm(sorted(os.listdir(input_path)))):
            input_path = Path(input_path)
            input_file = input_path/item
            extension = input_file.suffix
            if input_file.is_file():
                copy2(input_file, str(output_path /
                      ("{0:0{i}d}{1}".format(idx, extension, i=zero_pad))))


def main():
    args = parser.parse_args()
    input_path = Path(args.input)
    if args.output is not None:
        output_path = Path(args.output).absolute()
    else:
        output_path = input_path

    if input_path.is_file() or output_path.is_file():
        print("input path should be a folder")
        exit(1)

    renumber_images(input_path, output_path, args.zero_pad, create_dir=True)


if __name__ == "__main__":
    main()
