from pathlib import Path
from PIL import Image
import os
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="input directory of images")
parser.add_argument("-o", "--output", help="input directory of images")


def resize_images(final_size, input_path, output_path, create_dir=False):
    if create_dir:
        if not output_path.exists():
            os.mkdir(output_path)

    for item in tqdm(os.listdir(input_path)):
        if os.path.isfile(input_path+item):
            im = Image.open(input_path+item)
            im = im.resize(final_size)
            im = im.convert('RGB')
            im.save(str(Path(output_path/(item))),
                    'JPEG', quality=90)


def main():
    args = parser.parse_args()
    input_path = args.input
    output_path = Path(args.output).absolute()
    final_size = (854, 480)

    resize_images(final_size, input_path, output_path, create_dir=True)


if __name__ == "__main__":
    main()
