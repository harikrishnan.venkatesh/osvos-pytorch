# Package Includes
from __future__ import division

import os
import torch
from torch.utils.data import DataLoader

# Custom includes
from dataloaders import davis_2016 as db
from dataloaders import custom_transforms as tr
import networks.vgg_osvos as vo
from dataloaders.helpers import im_normalize
from mypath import Path
import argparse
import cv2
import numpy as np

from utils import sigmoid

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--category', default='blackswan')

args = parser.parse_args()

# Setting of parameters
seq_name = args.category

db_root_dir = Path.db_root_dir()
save_dir = Path.save_root_dir()

if not os.path.exists(save_dir):
    os.makedirs(os.path.join(save_dir))

vis_net = 0  # Visualize the network?
vis_res = 0  # Visualize the results?
nAveGrad = 5  # Average the gradient every nAveGrad iterations
nEpochs = 2000 * nAveGrad  # Number of epochs for training
snapshot = nEpochs  # Store a model every snapshot epochs
parentEpoch = 240

# Parameters in p are used for the name of the model
p = {
    'trainBatch': 4,  # Number of Images in each mini-batch
}
seed = 0

parentModelName = 'parent'
# Select which GPU, -1 if CPU
gpu_id = 0
device = torch.device("cuda:"+str(gpu_id)
                      if torch.cuda.is_available() else "cpu")

# Network definition
net = vo.OSVOS(pretrained=0)
net.load_state_dict(torch.load(os.path.join(save_dir, '{}_epoch-9999.pth'.format(seq_name)),
                               map_location=lambda storage, loc: storage))


net.to(device)


db_test = db.DAVIS2016(train=False, db_root_dir=db_root_dir,
                       transform=tr.ToTensor(), seq_name=seq_name)

testloader = DataLoader(db_test, batch_size=1, shuffle=False, num_workers=1)


# Testing Phase
if vis_res:
    import matplotlib.pyplot as plt
    plt.close("all")
    plt.ion()
    f, ax_arr = plt.subplots(1, 3)

save_dir_res = os.path.join(save_dir, 'Results', seq_name)
if not os.path.exists(save_dir_res):
    os.makedirs(save_dir_res)


print('Testing Network')
with torch.no_grad():  # PyTorch 0.4.0 style
    # Main Testing Loop
    for ii, sample_batched in enumerate(testloader):

        img, gt, fname = sample_batched['image'], sample_batched['gt'], sample_batched['fname']

        # Forward of the mini-batch
        inputs, gts = img.to(device), gt.to(device)

        outputs = net.forward(inputs)

        for jj in range(int(inputs.size()[0])):
            pred = np.transpose(
                outputs[-1].cpu().data.numpy()[jj, :, :, :], (1, 2, 0))
            pred = sigmoid(pred)
            pred = np.squeeze(pred)

            # Save the result, attention to the index jj
            cv2.imwrite(os.path.join(save_dir_res, os.path.basename(fname[jj]) + '.png'),
                        cv2.cvtColor(pred/pred.max()*255.0, cv2.COLOR_RGB2BGR))

            if vis_res:
                img_ = np.transpose(img.numpy()[jj, :, :, :], (1, 2, 0))
                gt_ = np.transpose(gt.numpy()[jj, :, :, :], (1, 2, 0))
                gt_ = np.squeeze(gt)
                # Plot the particular example
                ax_arr[0].cla()
                ax_arr[1].cla()
                ax_arr[2].cla()
                ax_arr[0].set_title('Input Image')
                ax_arr[1].set_title('Ground Truth')
                ax_arr[2].set_title('Detection')
                ax_arr[0].imshow(im_normalize(img_))
                ax_arr[1].imshow(gt_)
                ax_arr[2].imshow(im_normalize(pred))
                plt.pause(0.001)
