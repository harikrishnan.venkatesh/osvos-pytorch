# Package Includes
from __future__ import division

import os
import cv2
import tqdm
import torch
import argparse
import numpy as np
from pathlib import Path
from mypath import Path as data_paths
from utils import sigmoid
import networks.vgg_osvos as vo
from torchvision import transforms

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--category', default='blackswan')
parser.add_argument(
    '-i', '--input', help='input video path', required=True)
parser.add_argument("-m", "--model", help='model to use')


MEAN_VAL = (104.00699, 116.66877, 122.67892)


def resize_image_without_stretch(img, size=(854, 480)):

    h, w = img.shape[:2]
    c = img.shape[2] if len(img.shape) > 2 else 1

    if h == w:
        return cv2.resize(img, size, cv2.INTER_AREA)

    dif = h if h > w else w

    interpolation = cv2.INTER_AREA if dif > (
        size[0]+size[1])//2 else cv2.INTER_CUBIC

    x_pos = (dif - w)//2
    y_pos = (dif - h)//2

    if len(img.shape) == 2:
        mask = np.zeros((dif, dif), dtype=img.dtype)
        mask[y_pos:y_pos+h, x_pos:x_pos+w] = img[:h, :w]
    else:
        mask = np.zeros((dif, dif, c), dtype=img.dtype)
        mask[y_pos:y_pos+h, x_pos:x_pos+w, :] = img[:h, :w, :]

    return cv2.resize(mask, size, interpolation)


def resize_image(img, size=(854, 480)):
    return cv2.resize(img, size)


def preprocess_image(img):
    img = resize_image(img)

    img = np.array(img, dtype=np.float32)
    img = np.subtract(img, np.array(MEAN_VAL, dtype=np.float32))
    img = transforms.ToTensor()(img)
    img = torch.unsqueeze(img, 0)
    return img


def infer_video(args):

    # Setting of parameters
    seq_name = args.category
    save_dir = data_paths.save_root_dir()

    if not os.path.exists(save_dir):
        raise FileNotFoundError("Trained model not found")

    # Select which GPU, -1 if CPU
    gpu_id = 0
    device = torch.device("cuda:"+str(gpu_id)
                          if torch.cuda.is_available() else "cpu")

    # Network definition
    net = vo.OSVOS(pretrained=0)
    if args.model is not None:
        net.load_state_dict(torch.load(os.path.join(save_dir, args.model),
                                       map_location=lambda storage, loc: storage))
    else:
        net.load_state_dict(torch.load(os.path.join(save_dir, '{}_epoch-9999.pth'.format(seq_name)),
                                       map_location=lambda storage, loc: storage))
    net.to(device)

    vid = cv2.VideoCapture(args.input)
    vid_len = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
    fps = vid.get(cv2.CAP_PROP_FPS)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    vid_width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
    vid_height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
    video_input_path = Path(args.input)
    out = cv2.VideoWriter(str(video_input_path.parent/(video_input_path.stem+"_inference.avi")),
                          fourcc, fps, (854, 480))

    print('Testing Network')
    with torch.no_grad():  # PyTorch 0.4.0 style

        # Main Testing Loop
        with tqdm.tqdm(total=vid_len) as pbar:
            while True:
                _, img = vid.read()
                # NO BGR TO RGB is needed I think
                if img is None:
                    break

                # Forward of the mini-batch
                orig_img = resize_image(np.copy(img))
                img = preprocess_image(img)
                inputs = img.to(device)

                outputs = net.forward(inputs)

                pred = np.transpose(
                    outputs[-1].cpu().data.numpy()[0, :, :, :], (1, 2, 0))
                pred = sigmoid(pred)
                pred = np.squeeze(pred)
                pred = pred/pred.max()
                pred = pred*255
                pred = pred.astype(np.uint8)

                pred = cv2.cvtColor(pred, cv2.COLOR_GRAY2BGR)
                pred[:, :, 0] = 0
                pred[:, :, 2] = 0
                result = overlay_transparent(orig_img, pred)
                # cv2.imshow("sample",result)
                # cv2.waitKey(0)
                out.write(result)
                pbar.update(1)

        out.release()


def overlay_transparent(background_img, img_to_overlay, weight=0.5):
    return cv2.addWeighted(background_img, weight, img_to_overlay, 1-weight, 0)


if __name__ == '__main__':
    args = parser.parse_args()
    infer_video(args)
